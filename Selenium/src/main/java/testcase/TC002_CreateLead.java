package testcase;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	//@Test(invocationCount=2 , invocationTimeOut=30000)
	//@Test(groups="smoke")
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc =  "Create a new Lead";
		category = "smoke";
		author = "sethu";
		excelFileName = "CL";
	}
	@Test(dataProvider="fetchData")
	public void createLead(String cname, String fname, String lname) {
		
		WebElement eleCL=locateElement("linktext","Create Lead");
		click(eleCL);
		WebElement elecompname = locateElement("id", "createLeadForm_companyName");
		type(elecompname, cname);
		WebElement elefname = locateElement("id", "createLeadForm_firstName");
		type(elefname, fname);
		WebElement elelname = locateElement("id", "createLeadForm_lastName");
		type(elelname, lname);
		WebElement elesub=locateElement("name","submitButton");
		click(elesub);
		
	}
	
	@DataProvider(name="fetchData", indices = {2,3})
	public Object[][] readData() throws IOException {
	
		return  ReadExcel.getData("CL");
	  
	}
	
}








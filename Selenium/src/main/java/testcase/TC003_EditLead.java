package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethods.SeMethods;

public class TC003_EditLead extends SeMethods{
	
	
	//@Test(dependsOnMethods="testcase.TC002_CreateLead.createLead", alwaysRun=true) 
	@Test(groups="sanity", dependsOnGroups="smoke")
	public void editLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM=locateElement("linktext","CRM/SFA");
		click(eleCRM);	
		WebElement eleCL=locateElement("linktext","Leads");
		click(eleCL);
		WebElement eleFL=locateElement("linktext","Find Leads");
		click(eleFL);
		WebElement eleFirstname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstname, "senthel");
		WebElement elefinfbutton = locateElement("xpath", "//button[text()='Find Leads']");
        click(elefinfbutton);
        Thread.sleep(2000);
		WebElement elelname = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(elelname);
		WebElement eleEdit = locateElement("linktext", "Edit");
		click(eleEdit);
		WebElement eleUpdateCmp = locateElement("id", "updateLeadForm_companyName");
		type(eleUpdateCmp, "TL");
		WebElement elesub=locateElement("name","submitButton");
		click(elesub);
		driver.close();
		
	}
	
}








package testcase;

import org.testng.annotations.BeforeTest;

import wdMethods.ProjectMethods;

public class TC005_CreateLeadUpages extends ProjectMethods
{
@BeforeTest
public void setData()
{
	testCaseName = "TC005_CreateLeadUpages";
	testCaseDesc =  "Create a new Lead";
	category = "smoke";
	author = "Senthil";
	excelFileName = "CL";	
}
	
}

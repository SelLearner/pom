package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethods.SeMethods;

public class TC004_DeleteLead extends SeMethods{
	
	@Test(groups="regression")
	public void deleteLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM=locateElement("linktext","CRM/SFA");
		click(eleCRM);	
		WebElement eleCL=locateElement("linktext","Leads");
		click(eleCL);
		WebElement eleFL=locateElement("linktext","Find Leads");
		click(eleFL);
		WebElement eleFirstname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstname, "senthel");
		WebElement elefinfbutton = locateElement("xpath", "//button[text()='Find Leads']");
        click(elefinfbutton);
        Thread.sleep(2000);
        String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		WebElement elelname = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(elelname);
		WebElement eleDelete = locateElement("linktext", "Delete");
		click(eleDelete);
		WebElement eleFL1=locateElement("linktext","Find Leads");
		click(eleFL1);
		WebElement eleid = locateElement("xpath", "(//input[@name='id'])");
		type(eleid, text);
		WebElement elefn = locateElement("xpath", "//button[text()='Find Leads']");
        click(elefn);
        Thread.sleep(2000);
		WebElement eleVerify = locateElement("class", "x-paging-info");
		verifyExactText(eleVerify, "No records to display");
		driver.close();
	}
	
}








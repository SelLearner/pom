package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {

public CreateLead()	
{
	PageFactory.initElements(driver, this);	
}
@FindBy(id="createLeadForm_companyName")
WebElement eleCName;

public CreateLead typeCName(String data)
{
	type(eleCName,data);
	return this;
}
@FindBy(id="createLeadForm_firstName")
WebElement eleFName;

public CreateLead typeFName(String data)
{
	type(eleFName,data);
	return this;
}
@FindBy(id="createLeadForm_lastName")
WebElement eleLName;

public CreateLead typeLName(String data)
{
	type(eleLName,data);
	return this;
}
@FindBy(className="smallSubmit")
WebElement eleCreate;
public ViewLeads clickLogin()
{
	click(eleCreate);
	ViewLeads hp = new ViewLeads();
	return hp;


}
}
package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods {

public MyHome()
{
	
}
@FindBy(how=How.LINK_TEXT,using="Leads")
WebElement eleClead;
public MyLeads ClickClead()
{
	click(eleClead);
	return new MyLeads();
	
}
	
}
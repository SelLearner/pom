package sel.day1;

import java.util.Scanner;

class FloydTriangle
{
   public static void main(String args[])
   {
      int n, num = 1, c, d;
      Scanner scr = new Scanner(System.in);
 
      System.out.println("Enter the number of rows ");
      n = scr.nextInt();
 
      System.out.println("Floyd's triangle :-");
 
      for ( c = 1 ; c <= n ; c++ )
      {
         for ( d = 1 ; d <= c ; d++ )
         {
            System.out.print(num+" ");
            num++;
         }
         System.out.println();
}
   }
}
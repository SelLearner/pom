package sel.day1;

import java.util.List;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowsLearn {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","F:\\testleaf\\workspace\\Selenium\\Drivers\\chromedriver.exe");
		ChromeDriver cd=new ChromeDriver();
	    cd.get("https://www.irctc.co.in");	
	    cd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    cd.manage().window().maximize();
	    cd.findElementByXPath("//span[text()='AGENT LOGIN']").click();
        cd.findElementByLinkText("Contact Us").click();
        
        Set<String> windowHandles = cd.getWindowHandles();
        System.out.println(cd.getWindowHandles());
        List<String> listofWindows = new ArrayList<>();
        listofWindows.addAll(windowHandles);
        cd.switchTo().window(listofWindows.get(1));
        System.out.println(cd.getTitle());
        System.out.println(cd.getCurrentUrl());
        File src = cd.getScreenshotAs(OutputType.FILE);
        File des = new File("./snaps/img.png");  
        FileUtils.copyFile(src, des);
}
}

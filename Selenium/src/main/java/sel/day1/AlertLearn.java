package sel.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertLearn {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","F:\\testleaf\\workspace\\Selenium\\Drivers\\chromedriver.exe");
		ChromeDriver cd=new ChromeDriver();
	    cd.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	    cd.switchTo().frame("iframeResult");
	    cd.findElementByXPath("//button[text()='Try it']").click();
	    Alert alert = cd.switchTo().alert();
	    String text = alert.getText();
	    System.out.println(text);
	    //
	    alert.accept();
	    cd.findElementByXPath("//button[text()='Try it']").click();
	    alert.sendKeys("Hai");
	    alert.accept();
	    text=cd.findElementById("demo").getText();
	    System.out.println(text);
	    	

	}

}

package sel.day1;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class WebTest {

	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver","F:\\testleaf\\workspace\\Selenium\\Drivers\\chromedriver.exe");
	ChromeDriver cd=new ChromeDriver();
    cd.get("http://leaftaps.com/opentaps/control/main");
    cd.findElementById("username").sendKeys("DemoSalesManager");
	cd.findElementById("password").sendKeys("crmsfa");
	cd.findElementByClassName("decorativeSubmit").click();
	cd.findElementByLinkText("CRM/SFA").click();
	cd.findElementByLinkText("Create Lead").click();
	cd.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
	cd.findElementById("createLeadForm_firstName").sendKeys("Senthil");
	cd.findElementById("createLeadForm_lastName").sendKeys("kumar");
	WebElement src=cd.findElementById("createLeadForm_dataSourceId");
	Select dropDown=new Select(src);
	dropDown.selectByVisibleText("Direct Mail");
	WebElement src1=cd.findElementById("createLeadForm_marketingCampaignId");
	Select dropDown1=new Select(src1);
	List <WebElement> options=dropDown1.getOptions();
	for (WebElement eachOption : options)
	{
		System.out.println(eachOption.getText());
	}
	
	dropDown1.selectByIndex(options.size()-2);
	cd.findElementByClassName("smallSubmit").click();
	cd.close();
	cd.quit();
	}

}







package sel.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FaceBook {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "F:\\testleaf\\workspace\\Selenium\\Drivers\\chromedriver.exe");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		ChromeDriver cd = new ChromeDriver(ops);
		cd.get("https://www.facebook.com/");
		cd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		cd.manage().window().maximize();
		cd.findElementById("email").sendKeys("senthilsamhara@gmail.com");
		cd.findElementById("pass").sendKeys("fair@123");
		cd.findElementByXPath("//input[@type='submit']").click();
		cd.findElementByName("q").sendKeys("Test Leaf");
		cd.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		Thread.sleep(5000);
		String ncr = cd.findElementByXPath("(//button[@type=\"submit\"])[2]").getText();
		System.out.println(ncr);
		if (ncr.equals("Like")) {
			cd.findElementByXPath("//img[@class='_2yeu _6n07 img']").click();
			String scr = cd.findElementByXPath("//div[contains(text(),'people like this')]").getText();
			System.out.println(scr.replaceAll("\\D", ""));

		}
	}

}
